# Description

Rust implementation of [cryptopals problems](https://www.cryptopals.com/), sets 1 - 6. Code is organized in categories like attacks, ciphers, protocols and so on rather than single challenges. For testing particular challenge, it is necessary to implement driver code in `main.rs`.
