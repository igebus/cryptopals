const N: usize = 624;
const M: usize = 397;
const MATRIX_A: u32 = 0x9908b0dfu32;
const LOWER_MASK: u32 = 0x80000000u32;
const UPPER_MASK: u32 = 0x7fffffffu32;

pub struct MT19937 {
    mt: [u32; N],
    mti: usize,
}

const MT19937_DEFAULT: MT19937 = MT19937 {
    mt: [0; N],
    mti: N + 1,
};

impl Default for MT19937 {
    #[inline]
    fn default() -> Self {
        MT19937_DEFAULT
    }
}

impl MT19937 {
    pub fn seed(&mut self, s: u32) {
        self.mt[0] = s;
        self.mti = 1;
        while self.mti < N {
            self.mt[self.mti] = 1812433253u32
                .wrapping_mul(self.mt[self.mti - 1] ^ (self.mt[self.mti - 1] >> 30))
                + self.mti as u32;

            self.mti += 1;
        }
    }

    pub fn set_internal_state(&mut self, state: &[u32]) {
        if state.len() != N {
            panic!("Invalid state length");
        }
        for i in 0..N {
            self.mt[i] = state[i];
        }
        self.mti = N;
    }

    pub fn extract_number(&mut self) -> u32 {
        if self.mti >= N {
            if self.mti > N {
                panic!("Generator was never seeded");
            }
            self.twist();
        }

        let mut y: u32 = self.mt[self.mti];
        y ^= (y >> 11) & 0xffffffffu32;
        y ^= (y << 7) & 0x9d2c5680u32;
        y ^= (y << 15) & 0xefc60000u32;
        y ^= y >> 1;

        self.mti += 1;

        y
    }

    fn twist(&mut self) {
        for i in 0..N {
            let x = (self.mt[i] & UPPER_MASK) + (self.mt[(i+1) % N] & LOWER_MASK);
            let mut x_a = x >> 1;

            if x % 2 != 0 {
                x_a = x_a ^ MATRIX_A;
            }

            self.mt[i] = self.mt[(i + M) % N] ^ x_a;
        }

        self.mti = 0;
    }
}
