pub mod aes {
    use crate::ciphers::aes::BLOCKSIZE;
    use crate::ciphers;
    use crate::pkcs7;
    use crate::utils;

    pub fn ecb_detect_oracle_get(msg: &[u8]) -> Vec<u8> {
        use rand::Rng;

        let key = utils::random_bytes(BLOCKSIZE);
        let prefix = utils::random_bytes(rand::thread_rng().gen_range(5..=10));
        let suffix = utils::random_bytes(rand::thread_rng().gen_range(5..=10));

        let msg: Vec<u8> = prefix
            .iter()
            .chain(msg.iter().chain(suffix.iter()))
            .cloned()
            .collect();

        let mode = rand::random::<bool>();

        if mode {
            ciphers::aes::ecb_encrypt(&msg, &key)
        } else {
            let iv = utils::random_bytes(BLOCKSIZE);
            ciphers::aes::cbc_encrypt(&msg, &key, &iv)
        }
    }

    pub fn ecb_byte_at_time_simple_get(payload: &[u8], key: &[u8]) -> Vec<u8> {
        let header = base64::decode("Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK").unwrap();

        let msg: Vec<u8> = payload
            .iter()
            .chain(header.iter())
            .cloned()
            .collect();
        ciphers::aes::ecb_encrypt(&msg, &key)
    }

    pub fn ecb_byte_at_time_hard_get(payload: &[u8], key: &[u8]) -> Vec<u8> {
        use rand::Rng;

        let random_prefix = utils::random_bytes(rand::thread_rng().gen_range(1..=100));
        let target_bytes = base64::decode("Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK").unwrap();

        let mut msg: Vec<u8> = random_prefix;
        msg.extend(payload);
        msg.extend(target_bytes);

        ciphers::aes::ecb_encrypt(&msg, &key)
    }

    struct Profile {
        email: String,
        uid: usize,
        role: String,
    }

    // for ecb cut and paste attack
    impl Profile {
        pub fn profile_for(email: &str) -> Self {
            Self {
                email: email.to_string().replace(&['&', '='][..], ""),
                uid: 10,
                role: "user".to_string(),
            }
        }

        pub fn parse(data: &str) -> Self {
            let mut email = String::new();
            let mut uid = 0;
            let mut role = String::new();

            for field in data.split("&") {
                let tmp = field.split("=").collect::<Vec<&str>>();
                match tmp[0] {
                    "email" => {
                        email = tmp[1].to_string();
                    }
                    "uid" => {
                        uid = tmp[1].parse::<usize>().unwrap();
                    }
                    "role" => {
                        role = tmp[1].to_string();
                    }
                    _ => {}
                };
            }

            Self { email, uid, role }
        }

        pub fn encode(&self) -> String {
            format!("email={}&uid={}&role={}", self.email, self.uid, self.role)
        }
    }

    pub fn ecb_cut_and_paste_get(payload: &[u8], key: &[u8]) -> Vec<u8> {
        ciphers::aes::ecb_encrypt(
            Profile::profile_for(std::str::from_utf8(payload).unwrap())
                .encode()
                .as_bytes(),
            key,
        )
    }

    pub fn ecb_cut_and_paste_check(cipher: &[u8], key: &[u8]) -> bool {
        let result = Profile::parse(std::str::from_utf8(&ciphers::aes::ecb_decrypt(cipher, key)).unwrap());

        result.role == "admin"
    }


    pub fn cbc_bitflip_get(payload: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
        let payload: Vec<u8> = payload
            .iter()
            .map(|&x| match x {
                b';' => b"%3B".to_vec(),
                b'=' => b"%3D".to_vec(),
                _ => [x].to_vec(),
            })
            .flatten()
            .collect();

        let mut msg = b"comment1=cooking%20MCs;userdata=".to_vec();
        msg.extend(payload);
        msg.extend(b";comment2=%20like%20a%20pound%20of%20bacon");

        ciphers::aes::cbc_encrypt(&msg, &key, &iv)
    }

    pub fn cbc_bitflip_check(cipher: &[u8], key: &[u8], iv: &[u8]) -> bool {
        let result = ciphers::aes::cbc_decrypt(&cipher, &key, &iv);
        String::from_utf8_lossy(&result).contains(";admin=true;")
    }

    pub fn cbc_padding_oracle_get(key: &[u8]) -> (Vec<u8>, Vec<u8>) {
        use rand::seq::SliceRandom;

        let strings: Vec<&str> = vec![
            "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
            "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
            "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
            "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
            "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
            "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
            "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
            "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
            "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
            "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93",
        ];

        let msg = base64::decode(strings.choose(&mut rand::thread_rng()).unwrap()).unwrap();
        let iv = utils::random_bytes(BLOCKSIZE);

        (ciphers::aes::cbc_encrypt(&msg, &key, &iv), iv)
    }

    pub fn cbc_padding_oracle_check(cipher: &[u8], key: &[u8], iv: &[u8]) -> bool {
        pkcs7::check(&ciphers::aes::cbc_decrypt(&cipher, &key, &iv), BLOCKSIZE)
    }

    pub fn ctr_random_access_get(key: &[u8], nonce: u64) -> Vec<u8> {
        use std::fs;

        let msg = base64::decode(
            fs::read_to_string("data/25.txt")
                .and_then(|res| Ok(res.replace("\n", "")))
                .unwrap()
            ).unwrap();

        ciphers::aes::ctr(&msg, key, nonce)
    }

    pub fn ctr_random_access_edit(cipher: &[u8], key: &[u8], nonce: u64, offset: usize, plain_byte: u8) -> Vec<u8> {
        let mut plain = ciphers::aes::ctr(cipher, key, nonce);
        plain[offset] = plain_byte;

        ciphers::aes::ctr(&plain, key, nonce)
    }

    pub fn ctr_bitflip_get(payload: &[u8], key: &[u8], nonce: u64) -> Vec<u8> {
        let payload: Vec<u8> = payload
            .iter()
            .map(|&x| match x {
                b';' => b"%3B".to_vec(),
                b'=' => b"%3D".to_vec(),
                _ => [x].to_vec(),
            })
            .flatten()
            .collect();

        let mut msg = b"comment1=cooking%20MCs;userdata=".to_vec();
        msg.extend(payload);
        msg.extend(b";comment2=%20like%20a%20pound%20of%20bacon");

        ciphers::aes::ctr(&msg, &key, nonce)
    }

    pub fn ctr_bitflip_check(cipher: &[u8], key: &[u8], nonce: u64) -> bool {
        let result = ciphers::aes::ctr(&cipher, &key, nonce);
        String::from_utf8_lossy(&result).contains(";admin=true;")
    }

    pub fn cbc_iv_key_get(key: &[u8]) -> Vec<u8> {
        use rand::Rng;

        // generate random plaintext from valid ascii
        let msg: Vec<u8> = (0..3*BLOCKSIZE)
            .map(|_| rand::thread_rng().gen_range(32..127) as u8)
            .collect();

        ciphers::aes::cbc_encrypt(&msg, &key, &key)
    }

    pub fn cbc_iv_key_check(cipher: &[u8], key: &[u8]) -> Result<(), Vec<u8>> {
        let msg = ciphers::aes::cbc_decrypt(cipher, key, key);

        for i in 0..msg.len() {
            // check if plaintext is valid ascii
            if !(32..127).contains(&msg[i]) {
                // if not, raise error containing decrypted text
                return Err(msg);
            }
        }

        Ok(())
    }
}

pub mod rng {
    use crate::ciphers;
    use crate::utils;
    use crate::rng;

    pub fn random_timestamp_seed_get() -> u32 {
        use rand::Rng;
        use std::thread;
        use std::time;
        use std::time::SystemTime;

        let mut rng = rng::MT19937::default();

        thread::sleep(time::Duration::from_secs(rand::thread_rng().gen_range(10..=100)));

        let seed: u32 = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs() as u32;
        rng.seed(seed);

        thread::sleep(time::Duration::from_secs(rand::thread_rng().gen_range(10..=100)));

        rng.extract_number()
    }

    pub fn clone_mt19937_get() -> Vec<u32> {
        let mut rng = rng::MT19937::default();
        let seed = rand::random::<u32>();
        rng.seed(seed);

       (0..624)
            .map(|_| rng.extract_number())
            .collect()
    }

    pub fn break_stream_mt19937_get(payload: &[u8]) -> Vec<u8> {
        use rand::Rng;

        let seed = rand::random::<u16>();
        let random_prefix = utils::random_bytes(rand::thread_rng().gen_range(5..=10));

        let msg: Vec<u8> = random_prefix
            .iter()
            .chain(payload)
            .cloned()
            .collect();

        ciphers::rng::stream_mt19937(&msg, seed)
    }
}

pub mod rsa {
    use crate::hash;
    use crate::utils;
    use crate::ciphers;
    use num_bigint::BigUint;

    pub fn unpadded_message_recovery_oracle_get(pub_key: &(BigUint, BigUint)) -> (Vec<u8>, Vec<u8>) {
        let msg: Vec<u8> = utils::random_bytes(32);
        let cipher: Vec<u8> = ciphers::rsa::encrypt(&msg, pub_key);
        let hash: Vec<u8> = hash::sha1(&cipher);

        (cipher, hash)
    }

    pub fn unpadded_message_recovery_oracle_check(cipher: &[u8], hash: &[u8], priv_key: &(BigUint, BigUint)) -> Result<Vec<u8>, ()> {
        if hash::sha1(&cipher) == hash {
            Err(())
        } else {
            Ok(ciphers::rsa::decrypt(&cipher, priv_key))
        }
    }

    pub fn bleichenbacher_signature_forge_check(msg: &[u8], signature: &[u8], pub_key: &(BigUint, BigUint)) -> bool {
        let blocksize = 1024 / (u8::BITS as usize);
        // hardcode asn1 istead of compiling / parsing it
        let asn1_sha1: &[u8] = &[0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14];

        let mut decrypted_signature: Vec<u8> = ciphers::rsa::decrypt(signature, pub_key);
        // add lost leading zeros
        if decrypted_signature.len() < blocksize {
            decrypted_signature = vec![0x00; blocksize - decrypted_signature.len()]
                .iter()
                .chain(decrypted_signature.iter())
                .cloned()
                .collect();
        }

        // check padding
        if decrypted_signature[0] != 0x00 || decrypted_signature[1] != 0x01 {
            return false;
        }
        let mut i = 2;
        while decrypted_signature[i] == 0xff {
            i += 1;
        }
        if decrypted_signature[i] != 0x00 {
            return false;
        }
        i += 1;

        // skip checking asn1
        i += asn1_sha1.len();

        // extract hash from signature, but *not* check for right alignment
        let signature_hash: Vec<u8> = decrypted_signature[i..i+20].to_vec();

        hash::sha1(msg) == signature_hash
    }

    pub fn parity_oracle_check(cipher: &[u8], priv_key: &(BigUint, BigUint)) -> bool {
        ciphers::rsa::decrypt(cipher, priv_key).last().unwrap() & 1 == 0
    }

    pub fn bleichenbacher_padding_oracle_check(cipher: &[u8], priv_key: &(BigUint, BigUint), blocksize: usize) -> bool {
        let mut msg = ciphers::rsa::decrypt(cipher, priv_key);

        // restore leading zeros
        if msg.len() < blocksize {
            msg = vec![0x00; blocksize - msg.len()]
                .iter()
                .chain(msg.iter())
                .cloned()
                .collect();
        }

        // check for proper padding
        msg[0] == 0x00 && msg[1] == 0x02
    }
}

pub mod dsa {
    use num_bigint::BigUint;

    pub fn nonce_recovery_from_repeated_nonce_get() -> Vec<(Vec<u8>, BigUint, BigUint, BigUint)> {
        use std::fs;

        let data = fs::read_to_string("data/44.txt").unwrap();
        let mut result: Vec<(Vec<u8>, BigUint, BigUint, BigUint)> = Vec::new();

        let mut tmp: (Vec<u8>, BigUint, BigUint, BigUint) = (
            Vec::new(), BigUint::from(0_u32), BigUint::from(0_u32), BigUint::from(0_u32)
        );
        for line in data.split("\n") {
            let fields: Vec<&str> = line.split(": ").collect();
            let (key, value) = (fields[0], fields[1]);

            match key {
                "msg" => {
                    tmp.0 = value.as_bytes().to_vec();
                },
                "s" => {
                    tmp.1 = BigUint::parse_bytes(value.as_bytes(), 10).unwrap();
                },
                "r" => {
                    tmp.2 = BigUint::parse_bytes(value.as_bytes(), 10).unwrap();
                },
                "m" => {
                    tmp.3 = BigUint::parse_bytes(value.as_bytes(), 16).unwrap();
                    result.push(tmp);
                    tmp = (Vec::new(), BigUint::from(0_u32), BigUint::from(0_u32), BigUint::from(0_u32));
                },
                _ => {}
            }
        }

        result
    }
}
