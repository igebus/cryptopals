pub mod aes {
    use aes::cipher::generic_array::GenericArray;
    use aes::cipher::{BlockDecrypt, BlockEncrypt, KeyInit};
    use aes::Aes128;

    use crate::pkcs7;
    use crate::utils;

    pub const BLOCKSIZE: usize = 16;

    pub fn ecb_encrypt(msg: &[u8], key: &[u8]) -> Vec<u8> {
        let cipher_interface = Aes128::new_from_slice(&key).unwrap();

        let padded_msg = pkcs7::pad(&msg, BLOCKSIZE);
        let blocks: Vec<&[u8]> = padded_msg
            .chunks(BLOCKSIZE)
            .collect();

        let mut result: Vec<u8> = Vec::with_capacity(padded_msg.len());

        for i in 0..blocks.len() {
            let mut block_generic_array = GenericArray::clone_from_slice(&blocks[i]);
            cipher_interface.encrypt_block(&mut block_generic_array);
            let encrypted_block: Vec<u8> = block_generic_array
                .iter()
                .map(|&x| x as u8)
                .collect();

            result.extend(encrypted_block);
        }

        result
    }

    pub fn ecb_decrypt(msg: &[u8], key: &[u8]) -> Vec<u8> {
        let cipher_interface = Aes128::new_from_slice(&key).unwrap();

        let blocks: Vec<&[u8]> = msg
            .chunks(BLOCKSIZE)
            .collect();

        let mut result: Vec<u8> = Vec::with_capacity(msg.len());

        for i in 0..blocks.len() {
            let mut block_generic_array = GenericArray::clone_from_slice(&blocks[i]);
            cipher_interface.decrypt_block(&mut block_generic_array);
            let decrypted_block: Vec<u8> = block_generic_array
                .iter()
                .map(|&x| x as u8)
                .collect();

            result.extend(decrypted_block);
        }

        pkcs7::strip(&result, BLOCKSIZE)
    }

    pub fn cbc_encrypt(msg: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
        let cipher_interface = Aes128::new_from_slice(&key).unwrap();

        let padded_msg: Vec<u8> = pkcs7::pad(msg, BLOCKSIZE);
        let padded_iv: Vec<u8> = pkcs7::pad(iv, BLOCKSIZE)
            .iter()
            .take(BLOCKSIZE)
            .cloned()
            .collect();

        let mut prev_encrypted_block: Vec<u8> = padded_iv;
        let blocks: Vec<&[u8]> = padded_msg
            .chunks(BLOCKSIZE)
            .collect();

        let mut result: Vec<u8> = Vec::with_capacity(padded_msg.len());

        for i in 0..blocks.len() {
            let mut block_generic_array = GenericArray::clone_from_slice(
                &utils::xor(&blocks[i], &prev_encrypted_block)
            );
            cipher_interface.encrypt_block(&mut block_generic_array);
            let encrypted_block: Vec<u8> = block_generic_array
                .iter()
                .map(|&x| x as u8)
                .collect();

            prev_encrypted_block = encrypted_block.clone();
            result.extend(encrypted_block);
        }

        result
    }

    pub fn cbc_decrypt(msg: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
        let cipher_interface = Aes128::new_from_slice(&key).unwrap();

        let padded_iv: Vec<u8> = pkcs7::pad(iv, BLOCKSIZE)
            .iter()
            .take(BLOCKSIZE)
            .cloned()
            .collect();

        let mut prev_encrypted_block: Vec<u8> = padded_iv;
        let blocks: Vec<&[u8]> = msg
            .chunks(BLOCKSIZE)
            .collect();

        let mut result: Vec<u8> = Vec::with_capacity(msg.len());

        for i in 0..blocks.len() {
            let mut block_generic_array = GenericArray::clone_from_slice(&blocks[i]);
            cipher_interface.decrypt_block(&mut block_generic_array);
            let decrypted_block: Vec<u8> = utils::xor(
                &prev_encrypted_block,
                &block_generic_array
                    .iter()
                    .map(|&x| x as u8)
                    .collect::<Vec<u8>>()
            );

            prev_encrypted_block = blocks[i].to_vec();
            result.extend(decrypted_block);
        }

        pkcs7::strip(&result, BLOCKSIZE)
    }

    pub fn ctr(msg: &[u8], key: &[u8], nonce: u64) -> Vec<u8> {
        let cipher_interface = Aes128::new_from_slice(&key).unwrap();

        let mut counter: u64 = 0;

        let blocks: Vec<&[u8]> = msg
            .chunks(BLOCKSIZE)
            .collect();

        let mut result: Vec<u8> = Vec::with_capacity(msg.len());

        for i in 0..blocks.len() {
            let keystream: Vec<u8> = nonce
                .to_le_bytes()
                .iter()
                .chain(counter.to_le_bytes().iter())
                .map(|&x| x as u8)
                .collect();

            let mut block_generic_array = GenericArray::clone_from_slice(&keystream);
            cipher_interface.encrypt_block(&mut block_generic_array);
            let encrypted_block: Vec<u8> = utils::xor(
                &blocks[i],
                &block_generic_array
                    .iter()
                    .map(|&x| x as u8)
                    .collect::<Vec<u8>>()
            );

            counter += 1;
            result.extend(encrypted_block);
        }

        result
    }
}

pub mod xor {
    use crate::utils;

    pub fn repeated(msg: &[u8], key: &[u8]) -> Vec<u8> {
        utils::xor(
            msg,
            &key
                .iter()
                .cycle()
                .take(msg.len())
                .cloned()
                .collect::<Vec<u8>>()
        )
    }

    pub fn fixed(msg: &[u8], key: u8) -> Vec<u8> {
        repeated(msg, &[key])
    }
}

pub mod rng {
    use crate::utils;
    use crate::rng;

    pub fn stream_mt19937(msg: &[u8], seed: u16) -> Vec<u8> {
        let mut rng = rng::MT19937::default();
        rng.seed(seed as u32);

        let mut result: Vec<u8> = Vec::with_capacity(msg.len());

        for block in msg.chunks(4) {
            let keystream = rng.extract_number();
            result.extend(utils::xor(&block, &keystream.to_be_bytes()));
        }

        result
    }
}

pub mod rsa {
    use crate::utils;
    use num_bigint::{BigUint, RandBigInt};

    // p, q are primes with length of 1024 bits
    pub fn keygen() -> ((BigUint, BigUint), (BigUint, BigUint)) {
        keygen_custom_size(1024)
    }

    // generate keys with custom size
    pub fn keygen_custom_size(keysize: usize) -> ((BigUint, BigUint), (BigUint, BigUint)) {
        let mut p: BigUint = BigUint::from(1_u32);
        while !utils::miller_rabin(&p, 20) {
            p = rand::thread_rng().gen_biguint_range(
                &((BigUint::from(1_u32) << (keysize / 2 - 1)) + 1_u32),
                &(BigUint::from(1_u32) << (keysize / 2))
            );
        }
        let mut q: BigUint = BigUint::from(1_u32);
        while !utils::miller_rabin(&q, 20) {
            q = rand::thread_rng().gen_biguint_range(
                &((BigUint::from(1_u32) << (keysize / 2 - 1)) + 1_u32),
                &(BigUint::from(1_u32) << (keysize / 2))
            );
        }

        let n: BigUint = &p * &q;
        let et: BigUint = (p - 1_u32) * (q - 1_u32);

        // e should (?) be chosen better than random
        // e=3 makes math fast, but is small
        // e=65537 is suggested by some
        let mut e: BigUint = BigUint::from(0_u32);
        let mut d: BigUint = utils::modinv(&e, &et);
        while d == BigUint::from(0_u32) {
            e = rand::thread_rng().gen_biguint_below(&et);
            d = utils::modinv(&e, &et);
        }

        ((e, n.clone()), (d, n))
    }

    // fixed e=3 for some attacks
    pub fn keygen_small_e() -> ((BigUint, BigUint), (BigUint, BigUint)) {
        let mut p: BigUint = BigUint::from(1_u32);
        while !utils::miller_rabin(&p, 20)
                || (p.clone() - 1_u32) % BigUint::from(3_u32) == BigUint::from(0_u32) {
            p = rand::thread_rng().gen_biguint_below(&(BigUint::from(1_u32) << 1024));
        }
        let mut q: BigUint = BigUint::from(1_u32);
        while !utils::miller_rabin(&q, 20)
                || (q.clone() - 1_u32) % BigUint::from(3_u32) == BigUint::from(0_u32) {
            q = rand::thread_rng().gen_biguint_below(&(BigUint::from(1_u32) << 1024));
        }

        let n: BigUint = &p * &q;
        let et: BigUint = (p - 1_u32) * (q - 1_u32);

        let e: BigUint = BigUint::from(3_u32);
        let d: BigUint = utils::modinv(&e, &et);

        ((e, n.clone()), (d, n))
    }

    pub fn encrypt(msg: &[u8], pub_key: &(BigUint, BigUint)) -> Vec<u8> {
        let (e, n) = pub_key;
        BigUint::from_bytes_be(msg).modpow(&e, &n).to_bytes_be()
    }

    pub fn decrypt(msg: &[u8], priv_key: &(BigUint, BigUint)) -> Vec<u8> {
        let (d, n) = priv_key;
        BigUint::from_bytes_be(msg).modpow(&d, &n).to_bytes_be()
    }
}

pub mod dsa {
    use num_bigint::{BigUint, RandBigInt};
    use crate::hash;
    use crate::utils;

    pub fn params() -> (BigUint, BigUint, BigUint) {
        let p: BigUint = BigUint::from_bytes_be(&[128,0,0,0,0,0,0,0,137,225,133,82,24,160,231,218,195,129,54,255,175,167,46,218,120,89,242,23,30,37,230,94,172,105,140,23,2,87,139,7,220,42,16,118,218,36,28,118,198,45,55,77,131,137,234,90,239,253,50,38,160,83,12,197,101,243,191,107,80,146,145,57,235,234,192,79,72,195,200,74,251,121,109,97,229,164,249,168,253,168,18,171,89,73,66,50,199,210,180,222,181,10,161,142,233,225,50,191,168,90,196,55,77,127,144,145,171,195,208,21,239,200,113,165,132,71,27,177]);
        let q: BigUint = BigUint::from_bytes_be(&[244,244,127,5,121,75,37,97,116,187,166,233,179,150,167,112,126,86,60,91]);
        let g: BigUint = BigUint::from_bytes_be(&[89,88,201,211,137,139,34,75,18,103,44,11,152,224,108,96,223,146,60,184,188,153,157,17,148,88,254,245,56,184,250,64,70,200,219,83,3,157,182,32,192,148,201,250,7,126,243,137,181,50,42,85,153,70,167,25,3,249,144,241,247,224,224,37,226,215,247,207,73,74,255,26,4,112,245,182,76,54,182,37,160,151,241,101,31,231,117,50,53,86,254,0,179,96,140,136,120,146,135,132,128,233,144,65,190,96,26,98,22,108,166,137,75,221,65,167,5,78,200,159,117,107,169,252,149,48,34,145]);

        (p, q, g)
    }

    pub fn keygen(params: &(BigUint, BigUint, BigUint)) -> (BigUint, BigUint) {
        let (p, q, g) = params;

        let x: BigUint = rand::thread_rng().gen_biguint_range(&BigUint::from(1_u32), &q);
        let y: BigUint = g.modpow(&x, &p);

        (x, y)
    }

    pub fn sign(msg: &[u8], x: &BigUint, params: &(BigUint, BigUint, BigUint)) -> (BigUint, BigUint) {
        let (p, q, g) = params;

        let mut k: BigUint = rand::thread_rng().gen_biguint_range(&BigUint::from(1_u32), &q);
        let mut r: BigUint = g.modpow(&k, &p) % q;
        let mut s: BigUint = (utils::modinv(&k, &q) * (BigUint::from_bytes_be(&hash::sha1(msg)) + x * &r)) % q;

        while r == BigUint::from(0_u32) || s == BigUint::from(0_u32) {
            k = rand::thread_rng().gen_biguint_range(&BigUint::from(1_u32), &q);
            r = g.modpow(&k, &p) % q;
            s = (utils::modinv(&k, &q) * (BigUint::from_bytes_be(&hash::sha1(msg)) + x * &r)) % q;
        }

        (r, s)
    }

    pub fn verify(msg: &[u8], y: &BigUint, signature: &(BigUint, BigUint), params: &(BigUint, BigUint, BigUint)) -> bool {
        let (p, q, g) = params;
        let (r, s) = signature;

        let w: BigUint = utils::modinv(&s, &q);
        let u1: BigUint = (BigUint::from_bytes_be(&hash::sha1(msg)) * &w) % q;
        let u2: BigUint = (r * &w) % q;
        let v: BigUint = ((&g.modpow(&u1, &p) * &y.modpow(&u2, &p)) % p) % q;

        v == *r
    }
}
