#![allow(dead_code)]

mod pkcs7;
mod ciphers;
mod utils;
mod attacks;
mod oracles;
mod rng;
mod hash;
mod algorithms;

fn main() {
    //let (pub_key, priv_key) = ciphers::rsa::keygen();
    //println!("{priv_key:?} {pub_key:?}");
    //let msg = b"abc".to_vec();
    //let emsg = ciphers::rsa::encrypt(&msg, &pub_key);
    //let dmsg = ciphers::rsa::decrypt(&emsg, &priv_key);

    //println!("{msg:?}\n{emsg:?}\n{dmsg:?}");

    //attacks::rsa::parity_oracle();
    attacks::rsa::bleichenbacher_padding_oracle();
}
