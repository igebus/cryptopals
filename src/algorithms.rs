use num_bigint::{BigUint, RandBigInt};
use std::thread;
use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc::channel;

// different parties are represented by different threads
// communication is handled by channels
//
// could be easily rewritten for different network hosts and socket communication

pub fn diffie_hellman() {
    #![allow(non_snake_case)]

    //let p: BigUint = BigUint::from_bytes_le(b"ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff");
    let p: BigUint = BigUint::from(37_u32);
    let g: BigUint = BigUint::from(2_u32);

    let (tx_alice, rx_alice): (Sender<BigUint>, Receiver<BigUint>) = channel();
    let (tx_bob, rx_bob): (Sender<BigUint>, Receiver<BigUint>) = channel();

    let alice = thread::spawn({
        let (p, g) = (p.clone(), g.clone());
        move || {
            println!("alice\t(p, g): {p} {g}");

            let a: BigUint = rand::thread_rng().gen_biguint_below(&p);
            let A: BigUint = g.modpow(&a, &p);
            println!("alice\t(a, A): {a} {A}");

            tx_alice.send(A.clone()).unwrap();
            let B: BigUint = rx_bob.recv().unwrap();
            println!("alice\t(B): {B}");

            let s: BigUint = B.modpow(&a, &p);
            println!("alice\t(s): {s}");
        }
    });

    let bob = thread::spawn({
        let (p, g) = (p, g).clone();
        move || {
            println!("bob\t(p, g): {p} {g}");

            let b: BigUint = rand::thread_rng().gen_biguint_below(&p);
            let B: BigUint = g.modpow(&b, &p);
            println!("bob\t(b, B): {b} {B}");

            let A: BigUint = rx_alice.recv().unwrap();
            println!("bob\t(A): {A}");
            tx_bob.send(B.clone()).unwrap();

            let s: BigUint = A.modpow(&b, &p);
            println!("bob\t(s): {s}");
        }
    });

    alice.join().unwrap();
    bob.join().unwrap();
}

// should use sha256 instead of sha1
pub fn secure_remote_password() {
    #![allow(non_snake_case)]
    use crate::hash::{sha1, hmac_sha1};
    use crate::utils::random_bytes;

    let N: BigUint = BigUint::from(37_u32);
    let g: BigUint = BigUint::from(2_u32);
    let k: BigUint = BigUint::from(3_u32);

    let I: Vec<u8> = b"client identity".to_vec();
    let P: Vec<u8> = b"some secret password".to_vec();
    let s: Vec<u8> = random_bytes(16);
    let xH: Vec<u8> = sha1(
        &s
        .iter()
        .chain(P.iter())
        .cloned()
        .collect::<Vec<u8>>()
    );
    let x: BigUint = BigUint::from_bytes_le(&xH);
    let v: BigUint = g.modpow(&x, &N);

    let (tx_server_bignum, rx_server_bignum): (Sender<BigUint>, Receiver<BigUint>) = channel();
    let (tx_client_bignum, rx_client_bignum): (Sender<BigUint>, Receiver<BigUint>) = channel();
    let (tx_server_bytes, rx_server_bytes): (Sender<Vec<u8>>, Receiver<Vec<u8>>) = channel();
    let (tx_client_bytes, rx_client_bytes): (Sender<Vec<u8>>, Receiver<Vec<u8>>) = channel();

    let server = thread::spawn({
        let (N, g, k) = (N.clone(), g.clone(), k.clone());
        let (I, s, v) = (I.clone(), s.clone(), v.clone());
        move || {
            println!("server\t(N, g, k): {N} {g} {k}");
            println!("server\t(I, s, v): {I:?} {s:?} {v}");

            let I_c: Vec<u8> = rx_server_bytes.recv().unwrap();
            let A: BigUint = rx_server_bignum.recv().unwrap();
            println!("server\t(I_c, A): {I_c:?} {A}");

            if I != I_c {
                println!("wrong client id");
                return;
            }

            let b: BigUint = rand::thread_rng().gen_biguint_range(&BigUint::from(1_u32), &N);
            let B: BigUint = g.modpow(&b, &N) + &k * &v;
            println!("server\t(b, B): {b} {B}");

            tx_client_bytes.send(s.clone()).unwrap();
            tx_client_bignum.send(B.clone()).unwrap();

            let uH: Vec<u8> = sha1(
                &A
                .to_bytes_le()
                .iter()
                .chain(B.to_bytes_le().iter())
                .cloned()
                .collect::<Vec<u8>>()
            );
            let u: BigUint = BigUint::from_bytes_le(&uH);
            println!("server\t(u): {u}");

            let S: BigUint = (&A * &v.modpow(&u, &N)).modpow(&b, &N);
            let K: Vec<u8> = sha1(&S.to_bytes_le());
            println!("server\t(S, K): {S} {K:?}");

            let M: Vec<u8> = hmac_sha1(&K, &s);
            let M_c: Vec<u8> = rx_server_bytes.recv().unwrap();
            println!("server\t(M, M_c): {M:?} {M_c:?}");

            tx_client_bytes.send(
                if M == M_c {
                    [0x00].to_vec()
                } else {
                    [0x01].to_vec()
                }
            ).unwrap();
        }
    });

    let client = thread::spawn({
        let (N, g, k) = (N.clone(), g.clone(), k.clone());
        let (I, P) = (I.clone(), P.clone());
        move || {
            println!("client\t(N, g, k): {N} {g} {k}");
            println!("client\t(I, P): {I:?} {P:?}");

            let a: BigUint = rand::thread_rng().gen_biguint_range(&BigUint::from(1_u32), &N);
            let A: BigUint = g.modpow(&a, &N);
            // we can try to attack server by setting A = 0 mod N
            // let A: BigUint = BigUint::from(0_u32);
            println!("client\t(a, A): {a} {A}");

            tx_server_bytes.send(I.clone()).unwrap();
            tx_server_bignum.send(A.clone()).unwrap();

            let s: Vec<u8> = rx_client_bytes.recv().unwrap();
            let B: BigUint = rx_client_bignum.recv().unwrap();
            println!("client\t(s, B): {s:?} {B}");

            let xH: Vec<u8> = sha1(
                &s
                .iter()
                .chain(P.iter())
                .cloned()
                .collect::<Vec<u8>>()
            );
            let x: BigUint = BigUint::from_bytes_le(&xH);
            let uH: Vec<u8> = sha1(
                &A
                .to_bytes_le()
                .iter()
                .chain(B.to_bytes_le().iter())
                .cloned()
                .collect::<Vec<u8>>()
            );
            let u: BigUint = BigUint::from_bytes_le(&uH);
            println!("client\t(x, u): {x} {u}");

            let S: BigUint = (&N + &B - &k * g.modpow(&x, &N)).modpow(&(a + u * x), &N);
            // it will cause value of S to be 0 as well
            // let S: BigUint = BigUint::from(0_u32);
            let K: Vec<u8> = sha1(&S.to_bytes_le());
            println!("client\t(S, K): {S} {K:?}");

            let M: Vec<u8> = hmac_sha1(&K, &s);
            println!("client\t(M): {M:?}");

            tx_server_bytes.send(M.clone()).unwrap();

            let result: Vec<u8> = rx_client_bytes.recv().unwrap();

            if result == [0x00].to_vec() {
                println!("OK");
            } else {
                println!("ERR");
            }
        }
    });

    server.join().unwrap();
    client.join().unwrap();
}
