use num_bigint::{BigUint, BigInt, RandBigInt, Sign};

pub fn xor(bytes1: &[u8], bytes2: &[u8]) -> Vec<u8> {
    bytes1
        .iter()
        .zip(bytes2)
        .map(|(&b1, &b2)| b1 ^ b2)
        .collect()
}

pub fn random_bytes(amount: usize) -> Vec<u8> {
    (0..amount).map(|_| rand::random::<u8>()).collect()
}

pub fn nth_block(n: usize, msg: &[u8], blocksize: usize) -> Vec<u8> {
    msg
        .iter()
        .skip(n * blocksize)
        .take(blocksize)
        .cloned()
        .collect()
}

pub fn hamming_distance(bytes1: &[u8], bytes2: &[u8]) -> u32 {
    bytes1
        .iter()
        .zip(bytes2)
        .map(|(&b1, &b2)| (b1 ^ b2).count_ones() as u32)
        .sum()
}

const LETTER_FREQ: [f64; 27] = [
    0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015, // A-G
    0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749, // H-N
    0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758, // O-U
    0.00978, 0.02360, 0.00150, 0.01974, 0.00074, 0.19181, // V-Z & space char
];

pub fn score_text(msg: &[u8]) -> f64 {
    msg
        .iter()
        .map(|&b| match b as char {
            'a'..='z' => LETTER_FREQ[b as usize - 97],
            'A'..='Z' => LETTER_FREQ[b as usize - 65],
            ' ' => LETTER_FREQ[26],
            _ => 0.0
        })
        .sum()
}

pub fn read_be_u32(input: &[u8]) -> u32 {
    let (int_bytes, _rest) = input.split_at(std::mem::size_of::<u32>());
    u32::from_be_bytes(int_bytes.try_into().unwrap())
}

pub fn read_le_u32(input: &[u8]) -> u32 {
    let (int_bytes, _rest) = input.split_at(std::mem::size_of::<u32>());
    u32::from_le_bytes(int_bytes.try_into().unwrap())
}

pub fn miller_rabin(n: &BigUint, k: usize) -> bool {
    if *n == BigUint::from(2_u32) || *n == BigUint::from(3_u32) {
        return true;
    }

    if *n == BigUint::from(0_u32) || *n == BigUint::from(1_u32) || (n % 2_u32) == BigUint::from(0_u32) {
        return false;
    }

    let s: u64 = (n - 1_u32).trailing_zeros().unwrap();
    let d: BigUint = (n - 1_u32) >> s;
    for _ in 0..k {
        let a: BigUint = rand::thread_rng().gen_biguint_range(&BigUint::from(2_u32), &(n-2_u32));
        let mut x: BigUint = a.modpow(&d, &n);

        if x == BigUint::from(1_u32) || x == (n - 1_u32) {
            continue;
        }

        let mut flag: bool = false;
        for _ in 0..s-1 {
            x = x.modpow(&BigUint::from(2_u32), &n);
            if x == n - 1_u32 {
                flag = true;
                break;
            }
        }

        if !flag {
            return false;
        }
    }

    true
}

pub fn modinv(a: &BigUint, n: &BigUint) -> BigUint {
    let mut t: BigInt = BigInt::from(0);
    let mut r: BigInt = BigInt::from_biguint(Sign::Plus, n.clone());
    let mut newt: BigInt = BigInt::from(1);
    let mut newr: BigInt = BigInt::from_biguint(Sign::Plus, a.clone());

    while newr != BigInt::from(0) {
        let quotient: BigInt = r.clone() / newr.clone();
        (t, newt) = (newt.clone(), t - quotient.clone() * newt);
        (r, newr) = (newr.clone(), r - quotient * newr);
    }

    if r > BigInt::from(1) {
        return BigUint::from(0_u32)
    }

    if t < BigInt::from(0) {
        t += BigInt::from_biguint(Sign::Plus, n.clone());
    }

    t.to_biguint().unwrap()
}


// only for commincation with python
pub fn read_numbers_from_buffer(buffer: &[u8]) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();
    let mut current_number: String = String::new();

    for b in buffer {
        match *b as char {
            '0'..='9' => {
                current_number.push(*b as char);
            },
            '\n' => {
                if current_number.len() > 0 {
                    result.push(current_number);
                    current_number = String::new();
                }
            },
            _ => {
                break;
            }
        }
    }
    if current_number.len() > 0 {
        result.push(current_number);
    }

    return result;
}
