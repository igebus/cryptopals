use crate::utils;

pub fn sha1(msg: &[u8]) -> Vec<u8> {
    let state: [u32; 5] = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0];
    sha1_custom_state(msg, &state, msg.len() as u64)
}

pub fn sha1_custom_state(msg: &[u8], state: &[u32; 5], msg_length: u64) -> Vec<u8> {
    let mut h: [u32; 5] = state.clone();

    let msg_length = msg_length * (u8::BITS as u64);
    let mut padded_msg: Vec<u8> = msg.to_vec();
    padded_msg.push(0x80);
    padded_msg.extend(vec![0x00; (64 + (56 - padded_msg.len())) % 64]);
    padded_msg.extend(msg_length.to_be_bytes());

    for block in padded_msg.chunks(64) {
        let mut w: Vec<u32> = Vec::with_capacity(80);
        w.extend(block
            .chunks(4)
            .map(|x| utils::read_be_u32(x))
            .collect::<Vec<u32>>());
        for i in 16..80 {
            w.push((w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16]).rotate_left(1));
        }

        let (mut a, mut b, mut c, mut d, mut e) = (h[0], h[1], h[2], h[3], h[4]);

        for i in 0..80 {
            let (f, k): (u32, u32) =
                if i < 20 {
                    ((b & c) | ((!b) & d), 0x5a827999)
                } else if i < 40 {
                    (b ^ c ^ d, 0x6ed9eba1)
                } else if i < 60 {
                    ((b & c) | (b & d) | (c & d), 0x8f1bbcdc)
                } else {
                    (b ^ c ^ d, 0xca62c1d6)
                };

            let temp: u32 = a.rotate_left(5) + f + e + k + w[i];
            e = d;
            d = c;
            c = b.rotate_left(30);
            b = a;
            a = temp;
        }
        h[0] += a;
        h[1] += b;
        h[2] += c;
        h[3] += d;
        h[4] += e;
    }

    h
        .iter()
        .map(|x| x.to_be_bytes().to_vec())
        .flatten()
        .collect::<Vec<u8>>()
}

pub fn sha1_mac(key: &[u8], msg: &[u8]) -> Vec<u8> {
    sha1(&key
         .iter()
         .chain(msg)
         .cloned()
         .collect::<Vec<u8>>())
}

pub fn verify_sha1_mac(key: &[u8], msg: &[u8], mac: &[u8]) -> bool {
    sha1_mac(key, msg) == mac
}

pub fn md4(msg: &[u8]) -> Vec<u8> {
    let state: [u32; 4] = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476];
    md4_custom_state(msg, &state, msg.len() as u64)
}

pub fn md4_custom_state(msg: &[u8], state: &[u32; 4], msg_length: u64) -> Vec<u8> {
    let f = |x: u32, y: u32, z: u32| (x & y) | ((!x) & z);
    let g = |x: u32, y: u32, z: u32| (x & y) | (x & z) | (y & z);
    let h = |x: u32, y: u32, z: u32| (x ^ y ^ z);

    let ff = |a: u32, b: u32, c: u32, d: u32, x: u32, s: u32| {
        (a + f(b, c, d) + x).rotate_left(s)
    };
    let gg = |a: u32, b: u32, c: u32, d: u32, x: u32, s: u32| {
        (a + g(b, c, d) + x + 0x5a827999_u32).rotate_left(s)
    };
    let hh = |a: u32, b: u32, c: u32, d: u32, x: u32, s: u32| {
        (a + h(b, c, d) + x + 0x6ed9eba1_u32).rotate_left(s)
    };

    let mut state: [u32; 4] = state.clone();

    let msg_length = msg_length * (u8::BITS as u64);
    let mut padded_msg: Vec<u8> = msg.to_vec();
    padded_msg.push(0x80);
    padded_msg.extend(vec![0x00; (64 + (56 - padded_msg.len())) % 64]);
    padded_msg.extend(msg_length.to_le_bytes());

    let w: Vec<u32> = padded_msg
        .chunks(4)
        .map(|x| utils::read_le_u32(x))
        .collect();

    for x in w.chunks(16) {
        let (mut a, mut b, mut c, mut d) = (state[0], state[1], state[2], state[3]);

        a = ff(a, b, c, d, x[ 0],  3);
        d = ff(d, a, b, c, x[ 1],  7);
        c = ff(c, d, a, b, x[ 2], 11);
        b = ff(b, c, d, a, x[ 3], 19);
        a = ff(a, b, c, d, x[ 4],  3);
        d = ff(d, a, b, c, x[ 5],  7);
        c = ff(c, d, a, b, x[ 6], 11);
        b = ff(b, c, d, a, x[ 7], 19);
        a = ff(a, b, c, d, x[ 8],  3);
        d = ff(d, a, b, c, x[ 9],  7);
        c = ff(c, d, a, b, x[10], 11);
        b = ff(b, c, d, a, x[11], 19);
        a = ff(a, b, c, d, x[12],  3);
        d = ff(d, a, b, c, x[13],  7);
        c = ff(c, d, a, b, x[14], 11);
        b = ff(b, c, d, a, x[15], 19);

        a = gg(a, b, c, d, x[ 0],  3);
        d = gg(d, a, b, c, x[ 4],  5);
        c = gg(c, d, a, b, x[ 8],  9);
        b = gg(b, c, d, a, x[12], 13);
        a = gg(a, b, c, d, x[ 1],  3);
        d = gg(d, a, b, c, x[ 5],  5);
        c = gg(c, d, a, b, x[ 9],  9);
        b = gg(b, c, d, a, x[13], 13);
        a = gg(a, b, c, d, x[ 2],  3);
        d = gg(d, a, b, c, x[ 6],  5);
        c = gg(c, d, a, b, x[10],  9);
        b = gg(b, c, d, a, x[14], 13);
        a = gg(a, b, c, d, x[ 3],  3);
        d = gg(d, a, b, c, x[ 7],  5);
        c = gg(c, d, a, b, x[11],  9);
        b = gg(b, c, d, a, x[15], 13);

        a = hh(a, b, c, d, x[ 0],  3);
        d = hh(d, a, b, c, x[ 8],  9);
        c = hh(c, d, a, b, x[ 4], 11);
        b = hh(b, c, d, a, x[12], 15);
        a = hh(a, b, c, d, x[ 2],  3);
        d = hh(d, a, b, c, x[10],  9);
        c = hh(c, d, a, b, x[ 6], 11);
        b = hh(b, c, d, a, x[14], 15);
        a = hh(a, b, c, d, x[ 1],  3);
        d = hh(d, a, b, c, x[ 9],  9);
        c = hh(c, d, a, b, x[ 5], 11);
        b = hh(b, c, d, a, x[13], 15);
        a = hh(a, b, c, d, x[ 3],  3);
        d = hh(d, a, b, c, x[11],  9);
        c = hh(c, d, a, b, x[ 7], 11);
        b = hh(b, c, d, a, x[15], 15);

        state[0] += a;
        state[1] += b;
        state[2] += c;
        state[3] += d;
    }

    state
        .iter()
        .map(|x| x.to_le_bytes().to_vec())
        .flatten()
        .collect::<Vec<u8>>()
}

pub fn md4_mac(key: &[u8], msg: &[u8]) -> Vec<u8> {
    md4(&key
         .iter()
         .chain(msg)
         .cloned()
         .collect::<Vec<u8>>())
}

pub fn verify_md4_mac(key: &[u8], msg: &[u8], mac: &[u8]) -> bool {
    md4_mac(key, msg) == mac
}

pub fn hmac_sha1(key: &[u8], msg: &[u8]) -> Vec<u8> {
    const BLOCKSIZE: usize = 64;

    let mut key: Vec<u8> = key.to_vec();
    if key.len() > BLOCKSIZE {
        key = sha1(&key);
    }
    if key.len() < BLOCKSIZE {
        key.extend(vec![0x00; BLOCKSIZE - key.len()]);
    }

    let o_key_pad = utils::xor(&key, &[0x5c; BLOCKSIZE]);
    let i_key_pad = utils::xor(&key, &[0x36; BLOCKSIZE]);

    sha1_mac(&o_key_pad, &sha1_mac(&i_key_pad, msg))
}
