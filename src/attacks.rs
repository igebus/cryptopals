pub mod aes {
    use crate::ciphers::aes::BLOCKSIZE;
    use crate::ciphers;
    use crate::utils;
    use crate::oracles;

    pub fn ecb_detect(cipher: &[u8]) -> bool {
        let mut blocks: Vec<&[u8]> = cipher
            .chunks(BLOCKSIZE)
            .collect();
        blocks.sort();

        // if there are two identical blocks, it is aes ecb
        for i in 0..blocks.len() - 1 {
            if blocks[i] == blocks[i + 1] {
                return true;
            }
        }

        false
    }

    pub fn ecb_detect_oracle() -> bool {
        use oracles::aes::ecb_detect_oracle_get;

        // payload which result in occurence of identical blocks
        let payload: Vec<u8> = vec![0x00; 3 * BLOCKSIZE];
        let cipher = ecb_detect_oracle_get(&payload);

        ecb_detect(&cipher)
    }

    pub fn ecb_byte_at_time_simple() -> Vec<u8> {
        use oracles::aes::ecb_byte_at_time_simple_get;

        // assuming known blocksize (easy)
        // assuming that aes mode is ecb (easy)
        let key = utils::random_bytes(BLOCKSIZE);

        let length = ecb_byte_at_time_simple_get(b"", &key).len();
        let blocks = length / BLOCKSIZE;

        // init with zeroes
        let mut result: Vec<u8> = vec![0x00; BLOCKSIZE];
        let mut known_blocks = 0;

        while known_blocks < blocks {
            let mut known_bytes = 0;
            while known_bytes < BLOCKSIZE {
                // offset so we can attack bytes at any position
                let offset_payload: Vec<u8> = vec![0x00; BLOCKSIZE - 1 - known_bytes];
                // we are looking for known_blocks-th block of this cipher
                let sample = ecb_byte_at_time_simple_get(&offset_payload, &key);

                for b in 0x00..=0xff as u8 {
                    // fill blocksize - 1 last known bytes and attack the last byte
                    let mut payload: Vec<u8> = result[result.len() - BLOCKSIZE + 1..].to_vec();
                    payload.push(b);

                    let candidate = ecb_byte_at_time_simple_get(&payload, &key);

                    // check with sample
                    if utils::nth_block(0, &candidate, BLOCKSIZE)
                        == utils::nth_block(known_blocks, &sample, BLOCKSIZE) {
                        known_bytes += 1;
                        result.push(b);
                        break;
                    }

                    // should not happen
                    if b == 0xff {
                        known_bytes += 1;
                        result.push(0x00);
                    }
                }
            }
            known_blocks += 1;
        }

        // get rid of initial zeroes
        result[BLOCKSIZE..].to_vec()
    }

    fn ecb_byte_at_time_hard_check_offset(signature_blocks: usize, cipher: &[u8]) -> Option<usize> {
        let blocks: Vec<&[u8]> = cipher
            .chunks(BLOCKSIZE)
            .collect();

        // looking for signature, some number of identical block
        // breaks approx every 1 / blocksize times
        // when n last bytes of random prefix is equal to n last bytes of signature blocks
        for i in 0..blocks.len() - signature_blocks + 1 {
            let mut found = true;
            for j in i + 1..i + signature_blocks {
                if blocks[i] != blocks[j] {
                    found = false;
                    break;
                }
            }

            if found {
                return Some(i + signature_blocks);
            }
        }

        None
    }

    pub fn ecb_byte_at_time_hard() -> Vec<u8> {
        use oracles::aes::ecb_byte_at_time_hard_get;

        // slightly modified code from simple version
        let key = utils::random_bytes(BLOCKSIZE);

        let length = 144;
        let blocks = length / BLOCKSIZE;

        // number of blocks prepended to every payload
        let signature_blocks = 4;

        let mut result: Vec<u8> = vec![0x00; BLOCKSIZE];
        let mut known_blocks = 0;

        while known_blocks < blocks {
            let mut known_bytes = 0;
            while known_bytes < BLOCKSIZE {
                // signature blocks first
                let mut payload_offset: Vec<u8> = vec![0x01; signature_blocks * BLOCKSIZE];
                payload_offset.extend(vec![0x00; BLOCKSIZE - 1 - known_bytes]);
                let mut sample = ecb_byte_at_time_hard_get(&payload_offset, &key);

                // check whether random prefix does not break offset
                // it will work once every blocksize times (here 1/16)
                while ecb_byte_at_time_hard_check_offset(signature_blocks, &sample).is_none() {
                    sample = ecb_byte_at_time_hard_get(&payload_offset, &key);
                }
                let offset_sample = ecb_byte_at_time_hard_check_offset(signature_blocks, &sample).unwrap();

                for b in 0x00..=0xff as u8 {
                    let mut payload: Vec<u8> = vec![0x01; signature_blocks * BLOCKSIZE];
                    payload.extend(result[result.len() - BLOCKSIZE + 1..].to_vec());
                    payload.push(b);

                    let mut candidate = ecb_byte_at_time_hard_get(&payload, &key);
                    // same prefix checking
                    while ecb_byte_at_time_hard_check_offset(signature_blocks, &candidate).is_none() {
                        candidate = ecb_byte_at_time_hard_get(&payload, &key);
                    }
                    let offset_candidate = ecb_byte_at_time_hard_check_offset(signature_blocks, &candidate).unwrap();

                    // added required offset
                    if utils::nth_block(offset_candidate, &candidate, BLOCKSIZE)
                        == utils::nth_block(known_blocks + offset_sample, &sample, BLOCKSIZE) {
                        known_bytes += 1;
                        result.push(b);
                        break;
                    }

                    if b == 0xff {
                        known_bytes += 1;
                        result.push(0x00);
                    }
                }
            }
            known_blocks += 1;
        }

        result[BLOCKSIZE..].to_vec()
    }

    pub fn ecb_cut_and_paste() -> bool {
        use oracles::aes::ecb_cut_and_paste_get;
        use oracles::aes::ecb_cut_and_paste_check;

        let key = utils::random_bytes(BLOCKSIZE);

        // first part pads "email=" to blocksize
        let mut payload: Vec<u8> = vec![0x00; 10];
        // so second part can be cut as whole block
        payload.extend(b"admin           ");

        let cipher1 = ecb_cut_and_paste_get(&payload, &key);
        // payload2 has length 13 so it fills result to 32 bits
        let cipher2 = ecb_cut_and_paste_get(b"user@mail.com", &key);

        // cut and paste magic
        let mut result = utils::nth_block(0, &cipher2, BLOCKSIZE);
        result.extend(utils::nth_block(1, &cipher2, BLOCKSIZE));
        result.extend(utils::nth_block(1, &cipher1, BLOCKSIZE));

        ecb_cut_and_paste_check(&result, &key)
    }

    pub fn cbc_bitflip() -> bool {
        use oracles::aes::cbc_bitflip_get;
        use oracles::aes::cbc_bitflip_check;

        let key = utils::random_bytes(BLOCKSIZE);
        let iv = utils::random_bytes(BLOCKSIZE);

        let payload = b"9admin9true9    ".to_vec(); // payload is in third block

        let mut cipher = cbc_bitflip_get(&payload, &key, &iv);

        // editing single bits in second block
        cipher[BLOCKSIZE]    ^= 0b00000010; // one bit flip: '9' -> ';'
        cipher[BLOCKSIZE+6]  ^= 0b00000100; // one bit flip: '9' -> '='
        cipher[BLOCKSIZE+11] ^= 0b00000010; // one bit flip: '9' -> ';'

        cbc_bitflip_check(&cipher, &key, &iv)
    }

    pub fn cbc_padding_oracle() -> Vec<u8> {
        use oracles::aes::cbc_padding_oracle_get;
        use oracles::aes::cbc_padding_oracle_check;

        let key = utils::random_bytes(BLOCKSIZE);

        let (cipher, iv) = cbc_padding_oracle_get(&key);

        let mut payload: Vec<u8> = iv.clone();
        payload.extend(cipher.clone());

        let mut length = payload.len();

        // binary search to find out padding
        let mut low = length - 2 * BLOCKSIZE - 1;
        let mut high = length - BLOCKSIZE;

        while low + 1 < high {
            let mid = (low + high) / 2;

            payload[mid] ^= 0b00000001; // flip one bit

            // check whether padding is still correct reaction
            if cbc_padding_oracle_check(&payload[BLOCKSIZE..], &key, &payload[..BLOCKSIZE]) {
                low = mid;
            } else {
                high = mid;
            }

            payload[mid] ^= 0b00000001; // revert bit flip
        }

        let mut padding_offset = length - (low + BLOCKSIZE + 1);
        let mut i = length - padding_offset - 1;

        let mut current_padding = padding_offset as u8;

        let mut result: Vec<u8> = Vec::new();

        // decrypt everything but iv
        while i >= BLOCKSIZE {
            if padding_offset >= BLOCKSIZE {
                // if whole block is padded, cut it off
                length -= BLOCKSIZE;
                padding_offset -= BLOCKSIZE;
                payload = iv.clone();
                payload.extend(cipher[..length-BLOCKSIZE].to_vec());
                current_padding = 0;
            } else {
                 // else modify padding to be one longer
                let b: u8 = current_padding ^ (current_padding + 1);
                for j in i + 1..length {
                    payload[j - BLOCKSIZE] ^= b;
                }
            }

            for b in 0x00..=0xff {
                payload[i - BLOCKSIZE] ^= b; // attack last non padding byte

                if cbc_padding_oracle_check(&payload[BLOCKSIZE..], &key, &payload[..BLOCKSIZE]) {
                    result.push((current_padding + 1) ^ b);
                    padding_offset += 1;
                    current_padding += 1;
                    break;
                }

                payload[i - BLOCKSIZE] ^= b;
            }

            i -= 1;
        }

        result
            .iter()
            .rev()
            .cloned()
            .collect()
    }

    pub fn ctr_fixed_nonce(strings: Vec<&[u8]>) {
        // better (statistical version) in next function

        // randomly generated key, fixed
        let key = vec![211, 202, 93, 107, 113, 100, 163, 190, 208, 108, 51, 116, 45, 115, 99, 202];
        let nonce: u64 = 0;

        let mut ciphers: Vec<Vec<u8>> = strings
            .iter()
            .map(|x| ciphers::aes::ctr(&x, &key, nonce))
            .collect();

        // first two bytes
        /*
           for b1 in 0x00..=0xff as u8 {
               for b2 in 0x00..=0xff as u8 {
                   let mut score: f64 = 0.0;
                   for i in 0..ciphers.len() {
                       ciphers[i][0] ^= b1;
                       ciphers[i][1] ^= b2;
                       score += score_string(&String::from_utf8_lossy(&ciphers[i]));
                       ciphers[i][0] ^= b1;
                       ciphers[i][1] ^= b2;
                   }
                   println!("{score} {b1} {b2}");
               }
           }
       */

        let result: Vec<u8> = vec![251, 158, 14, 53, 23, 75, 6, 135, 0, 83, 124, 131, 220, 206, 222, 47, 100, 1, 146];
        // ...
        // guessing byte by byte by hand

        for i in 0..ciphers.len() {
            for j in 0..std::cmp::min(result.len(), ciphers[i].len()) {
                ciphers[i][j] ^= result[j];
            }
            println!("{}", String::from_utf8_lossy(&ciphers[i]));
        }

        // incremental guessing next char with fixed key
    }

    pub fn ctr_fixed_nonce_stat(strings: Vec<&[u8]>) -> Vec<u8> {
        let key = utils::random_bytes(BLOCKSIZE);
        let nonce = 0;

        let ciphers: Vec<Vec<u8>> = strings
            .iter()
            .map(|x| ciphers::aes::ctr(&x, &key, nonce))
            .collect();

        // set keysize to minimum cipher length
        let keysize = ciphers
            .iter()
            .map(|x| x.len())
            .min()
            .unwrap();

        // truncate cipherstexts
        let msg: Vec<u8> = ciphers
            .iter()
            .map(|x| x[..keysize].to_vec())
            .flatten()
            .collect();

        // solve statistically as repeated xor
        // this is fast, but may be inaccurate
        // mistakes can be fixed by hand
        let result: Vec<u8> = crate::attacks::xor::repeated_stat(&msg, keysize);

        result
    }

    pub fn ctr_random_access() -> Vec<u8> {
        use oracles::aes::ctr_random_access_get;
        use oracles::aes::ctr_random_access_edit;

        let key = utils::random_bytes(BLOCKSIZE);
        let nonce = rand::random::<u64>();

        let cipher = ctr_random_access_get(&key, nonce);

        let mut keystream: Vec<u8> = Vec::with_capacity(cipher.len());
        for offset in 0..cipher.len() {
            let mut key_byte: u8 = 0x00;
            loop {
                // if selected byte xored with keystream is 0
                // it is a byte of keystream
                if ctr_random_access_edit(&cipher, &key, nonce, offset, key_byte)[offset] == 0x00 {
                    break;
                }
                key_byte += 1;
            }

            keystream.push(key_byte);
        }

        // decrypt like plain xor
        utils::xor(&cipher, &keystream)
    }

    pub fn ctr_bitflip() -> bool {
        // almost same code as cbc bitflip attack ...
        use oracles::aes::ctr_bitflip_get;
        use oracles::aes::ctr_bitflip_check;

        let key = utils::random_bytes(BLOCKSIZE);
        let nonce = rand::random::<u64>();

        let payload = b"9admin9true9    ".to_vec(); // payload is in third block

        let mut cipher = ctr_bitflip_get(&payload, &key, nonce);

        // editing single bits in *third* block
        // ... but ctr bitflip works in same block
        cipher[2 * BLOCKSIZE]    ^= 0b00000010; // one bit flip: '9' -> ';'
        cipher[2 * BLOCKSIZE+6]  ^= 0b00000100; // one bit flip: '9' -> '='
        cipher[2 * BLOCKSIZE+11] ^= 0b00000010; // one bit flip: '9' -> ';'

        ctr_bitflip_check(&cipher, &key, nonce)
    }

    pub fn cbc_iv_key() -> bool {
        use oracles::aes::cbc_iv_key_get;
        use oracles::aes::cbc_iv_key_check;

        let key = utils::random_bytes(BLOCKSIZE);

        let cipher: Vec<u8> = cbc_iv_key_get(&key);

        // c0 - first cipher block
        // p0 - first plaintext block
        // k - key (= iv)
        // create payload: [c0, 0..0, c0]
        let mut payload: Vec<u8> = utils::nth_block(0, &cipher, BLOCKSIZE);
        payload.extend(vec![0x00; BLOCKSIZE]);
        payload.extend(utils::nth_block(0, &cipher, BLOCKSIZE));

        // to obtain error with plaintext: [p0 ^ k, ..., p0]
        // so we can xor first and third block to recover key
        if let Err(plaintext) = cbc_iv_key_check(&payload, &key) {
            let result_key = utils::xor(
                &utils::nth_block(0, &plaintext, BLOCKSIZE),
                &utils::nth_block(2, &plaintext, BLOCKSIZE)
            );

            result_key == key
        } else {
            false
        }
    }
}

pub mod xor {
    use crate::utils;
    use crate::ciphers;

    pub fn fixed_stat(cipher: &[u8]) -> u8 {
        let mut best_result = (f64::MIN, u8::MIN);

        for key in 0x00..=0xff as u8 {
            let msg = ciphers::xor::fixed(cipher, key);
            let score = utils::score_text(&msg);

            if score > best_result.0 {
                best_result = (score, key);
            }
        }

        best_result.1
    }

    pub fn repeated_stat(cipher: &[u8], keysize: usize) -> Vec<u8> {
        let mut result: Vec<u8> = Vec::with_capacity(keysize);

        for i in 0..keysize {
            let msg: Vec<u8> = cipher
                .iter()
                .skip(i)
                .step_by(keysize)
                .cloned()
                .collect();

            result.push(fixed_stat(&msg));
        }

        result
    }

    pub fn repeated_stat_keysize(cipher: &[u8]) -> Vec<Vec<u8>> {
        let keysize_range = 2..40;
        let hamming_test_blocks = 3;
        let results_number = 3;

        let mut keysize_scores: Vec<(usize, f64)> = Vec::with_capacity(keysize_range.len());

        for keysize in keysize_range {
            let blocks1: Vec<u8> = cipher
                .iter()
                .take(hamming_test_blocks * keysize)
                .cloned()
                .collect();

            let blocks2: Vec<u8> = cipher
                .iter()
                .skip(hamming_test_blocks * keysize)
                .take(hamming_test_blocks * keysize)
                .cloned()
                .collect();

            let normalized_hamming_score = utils::hamming_distance(&blocks1, &blocks2) as f64 / (hamming_test_blocks * keysize) as f64;
            keysize_scores.push((keysize, normalized_hamming_score));
        }

        keysize_scores.sort_by(|&(_, x), &(_, y)| x.partial_cmp(&y).unwrap());

        let mut results: Vec<Vec<u8>> = Vec::with_capacity(results_number);
        for (keysize, _) in &keysize_scores[..results_number] {
            results.push(repeated_stat(&cipher, *keysize));
        }

        results
    }
}

pub mod rng {
    use crate::ciphers;
    use crate::rng;
    use crate::oracles;

    pub fn random_timestamp_seed() -> u32 {
        use std::time::SystemTime;
        use oracles::rng::random_timestamp_seed_get;

        let mut rng = rng::MT19937::default();

        let random_number = random_timestamp_seed_get();
        let mut seed: u32 = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs() as u32;

        // checking seed from current time backwards
        while seed > 0 {
            rng.seed(seed);
            if rng.extract_number() == random_number {
                break;
            }
            seed -= 1;
        }

        seed
    }

    fn untemper_right_shift_xor(x: u32, shift: u32) -> u32 {
        let mut y: u32 = 0;
        for i in (0..32).rev() {
            y += (((x >> i) & 1) ^ ((y >> (i+shift)) & 1)) << i;
        }
        y
    }

    fn untemper_left_shift_xor(x: u32, shift: u32, mask: u32) -> u32 {
        let mut y: u32 = 0;
        for i in 0..shift {
            y += ((x >> i) & 1) << i;
        }
        for i in shift..32 {
            if (mask >> i) & 1 == 0 {
                y += ((x >> i) & 1) << i;
            } else {
                y += (((x >> i) & 1) ^ ((y >> (i-shift)) & 1)) << i;
            }
        }
        y
    }

    fn clone_mt19937_untemper_output(x: u32) -> u32 {
        let mut result: u32 = x;

        // reverting operations done internally by mt19937
        result = untemper_right_shift_xor(result, 1);
        result = untemper_left_shift_xor(result, 15, 0xefc60000);
        result = untemper_left_shift_xor(result, 7, 0x9d2c5680);
        result = untemper_right_shift_xor(result, 11);

        result
    }

    pub fn clone_mt19937() -> rng::MT19937 {
        // this attack would have been impossible if mt19937
        // had used irreversible operations

        use oracles::rng::clone_mt19937_get;

        // get all 624 tempered outputs
        let rng_output = clone_mt19937_get();
        // untemper them
        let internal_rng_state: Vec<u32> = rng_output
            .iter()
            .map(|&x| clone_mt19937_untemper_output(x))
            .collect();

        // and feed new rng with them
        let mut cloned_rng = rng::MT19937::default();
        cloned_rng.set_internal_state(&internal_rng_state);

        cloned_rng
    }

    pub fn break_stream_mt19937() -> u16 {
        use oracles::rng::break_stream_mt19937_get;

        let msg = b"aaaaaaaaaaaaaa".to_vec();
        let cipher = break_stream_mt19937_get(&msg);

        let mut seed: u16 = 0;
        while seed <= u16::MAX {
            let plain = ciphers::rng::stream_mt19937(&cipher, seed)[cipher.len()-msg.len()..].to_vec();
            if plain == msg {
                break;
            }
            seed += 1;
        }

        seed
    }
}

pub mod mac {
    use crate::utils;
    use crate::hash;

    fn generate_sha1_glue_padding(msg_length: u64) -> Vec<u8> {
        let mut padding: Vec<u8> = Vec::new();
        padding.push(0x80);
        padding.extend(vec![0x00; (64 + (56 - (msg_length + 1) as usize)) % 64]);
        padding.extend((msg_length * (u8::BITS as u64)).to_be_bytes());

        padding
    }

    fn generate_md4_glue_padding(msg_length: u64) -> Vec<u8> {
        let mut padding: Vec<u8> = Vec::new();
        padding.push(0x80);
        padding.extend(vec![0x00; (64 + (56 - (msg_length + 1) as usize)) % 64]);
        padding.extend((msg_length * (u8::BITS as u64)).to_le_bytes());

        padding
    }

    pub fn sha1_mac_length_extension() -> bool {
        use rand::Rng;

        let key = utils::random_bytes(rand::thread_rng().gen_range(5..=20));
        let msg = b"comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon";
        let payload = b";admin=true";

        let original_mac = hash::md4_mac(&key, msg);
        // extract state of sha1 from original mac
        //
        // feeding it to sha1 would have same effect
        // as additional chunk of text to hash
        let mut internal_state: [u32; 5] = [0; 5];
        original_mac
            .chunks(4)
            .map(|x| utils::read_be_u32(x))
            .enumerate()
            .for_each(|(i, x)| internal_state[i] = x);

        // test different key lengths
        for key_length in 1..30 as u64 {
            // generate same padding as sha1 would do
            let glue_padding: Vec<u8> = generate_sha1_glue_padding(msg.len() as u64 + key_length);
            // message we are trying to authenticate
            let forged_msg: Vec<u8> = msg
                .iter()
                .chain(glue_padding.iter())
                .chain(payload.iter())
                .cloned()
                .collect();

            // test
            let forged_mac = hash::sha1_custom_state(
                payload,
                &internal_state,
                key_length + forged_msg.len() as u64
            );

            if hash::verify_sha1_mac(&key, &forged_msg, &forged_mac) {
                return true;
            }
        }

        false
    }

    pub fn md4_mac_length_extension() -> bool {
        use rand::Rng;

        let key = utils::random_bytes(rand::thread_rng().gen_range(5..=20));
        let msg = b"comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon";
        let payload = b";admin=true";

        let original_mac = hash::md4_mac(&key, msg);
        // extract state of md4 from original mac
        //
        // feeding it to md4 would have same effect
        // as additional chunk of text to hash
        let mut internal_state: [u32; 4] = [0; 4];
        original_mac
            .chunks(4)
            .map(|x| utils::read_le_u32(x))
            .enumerate()
            .for_each(|(i, x)| internal_state[i] = x);

        // test different key lengths
        for key_length in 1..30 as u64 {
            // generate same padding as md4 would do
            let glue_padding: Vec<u8> = generate_md4_glue_padding(msg.len() as u64 + key_length);
            // message we are trying to authenticate
            let forged_msg: Vec<u8> = msg
                .iter()
                .chain(glue_padding.iter())
                .chain(payload.iter())
                .cloned()
                .collect();

            // test
            let forged_mac = hash::md4_custom_state(
                payload,
                &internal_state,
                key_length + forged_msg.len() as u64
            );

            if hash::verify_md4_mac(&key, &forged_msg, &forged_mac) {
                return true;
            }
        }

        false
    }

    // measuring time function for hmac_sha1_time_leak
    fn get_response_and_time(file: &[u8], signature: &[u8], prefix_len: usize) -> Result<(), f64> {
        use std::process::Command;
        use std::time::Instant;

        // possible http response status codes
        const CODE_OK: &[u8] = b"200";
        const CODE_ERR: &[u8] = b"500";

        // signature needs to be hex encoded
        // adding "x" as termination char and padding to 40 (hex length of sha1 digest)
        let url = |file: &[u8], signature: &[u8]|
            format!("localhost:9000/test?file={}&signature={}",
                &String::from_utf8_lossy(file),
                hex::encode(signature)[..prefix_len].chars().take(prefix_len).collect::<String>()
                    + &(prefix_len..40).map(|_| "x").collect::<String>()
            );

        let n = 30;
        let mut response_time: f64 = 0.0;

        for _i in 0..n {
            let now = Instant::now();

            // send http get request, get http status code
            let response = Command::new("curl")
                .args(&["-s", "-w", "%{http_code}", &url(file, signature)])
                .output()
                .unwrap()
                .stdout;

            response_time += now.elapsed().as_millis() as f64;

            match response.as_ref() {
                CODE_OK => {
                    return Ok(());
                },
                CODE_ERR => {},
                _ => {}
            }

        }

        Err(response_time / n as f64)
    }

    // server for hmac_sha1 time leak attack is written in python
    // under "python/" directory
    // function below is communicating with it using "curl" command
    pub fn hmac_sha1_time_leak() -> Vec<u8> {
        use rand::Rng;

        // because of laziness, assume that file is 16 lowercase letters
        // this way we avoid implementing url quoting
        let file: Vec<u8> = (0..16)
            .map(|_| rand::thread_rng().gen_range(97..=122) as u8)
            .collect();

        let mut result: Vec<u8> = vec![0x00; 20];
        let mut prefix_length: usize = 0;
        // get 0 prefix response time
        let mut last_response_time: f64 = get_response_and_time(&file, &result, prefix_length).unwrap_err();
        // 90% of server timeout on byte works (tested for 2ms and above)
        let expected_time_diff: f64 = 1.8;

        // another soultion is to check every half byte n cycles (n*16 times), and select
        // one with max response time, and after that check if response time prolonged
        // correctly (by factor or expected time diff)

        while prefix_length <= 40 {
            if prefix_length == 40 {
                if get_response_and_time(&file, &result, prefix_length).is_ok() {
                    break;
                } else {
                    prefix_length = 0;
                }
            }
            // we are guessing by half byte at time (hex encoding)
            let i: usize = prefix_length / 2;
            let step: u8 =
                if prefix_length % 2 == 0 {
                    0x10
                } else {
                    0x01
                };

            loop {
                let response = get_response_and_time(&file, &result, prefix_length + 1);
                match response {
                    // we guessed whole signature
                    Ok(_) => {
                        break;
                    },
                    Err(response_time) => {
                        let time_diff = response_time - last_response_time;
                        // check if we correctly guessed next half byte
                        if time_diff > expected_time_diff {
                            last_response_time = response_time;
                            break;
                        }
                    }
                }
                result[i] += step;
                if step == 0x01 && result[i] % 0x10 == 0x00 {
                    result[i] -= 0x10;
                }
            }

            prefix_length += 1;
        }

        result
    }
}

pub mod algorithms {
    use num_bigint::{BigUint, RandBigInt};
    use crate::utils;
    use crate::hash;
    use crate::ciphers;

    // networking part for mitm dh parameter injection is written in python
    // under "python/" directory
    pub fn diffie_hellman_mitm_parameter_injection() {
        #![allow(non_snake_case)]

        use std::io::prelude::*;
        use std::net::TcpListener;
        use ciphers::aes;

        let mut buffer: Vec<u8> = vec![0; 8192];

        // accept connection from both parties
        let listener_a = TcpListener::bind("127.0.0.1:9003").unwrap();
        let (mut connection_a, _) = listener_a.accept().unwrap();

        let listener_b = TcpListener::bind("127.0.0.1:9004").unwrap();
        let (mut connection_b, _) = listener_b.accept().unwrap();

        // sniff Alice's p, g, A
        connection_a.read(&mut buffer).unwrap();
        let mut v: Vec<BigUint> = utils::read_numbers_from_buffer(&buffer)
            .iter()
            .map(|x| x.parse::<BigUint>().unwrap())
            .collect();
        let (p, g, _A) = (v[0].clone(), v[1].clone(), v[2].clone());

        // send to Bob p, g, p (instead of A)
        buffer = format!("{}\n{}\n{}\n", p, g, p).as_bytes().to_vec();
        connection_b.write(&buffer).unwrap();

        // sniff Bob's B
        connection_b.read(&mut buffer).unwrap();
        v = utils::read_numbers_from_buffer(&buffer)
            .iter()
            .map(|x| x.parse::<BigUint>().unwrap())
            .collect();
        let _B = v[0].clone();

        // send to Alice p (instead of B)
        buffer = format!("{}\n", p).as_bytes().to_vec();
        connection_b.write(&buffer).unwrap();

        // now we know, that shared secret s = 0, because of A = B = p
        let s: BigUint = BigUint::from(0_u32);

        // generate key same way as Alice and Bob does
        let key: Vec<u8> = hash::sha1(format!("{s}").as_bytes())[..aes::BLOCKSIZE].to_vec();

        // sniff Alice's encrypted message and iv
        connection_a.read(&mut buffer).unwrap();
        // send to Bob
        connection_b.write(&buffer).unwrap();

        let iv_a: Vec<u8> = buffer[..aes::BLOCKSIZE].to_vec();
        let msg_a: Vec<u8> = buffer[aes::BLOCKSIZE..2 * aes::BLOCKSIZE].to_vec();

        // decrypt it
        let _plain_a = aes::cbc_decrypt(&msg_a, &key, &iv_a);

        // sniff Bob's encrypted message and iv
        connection_b.read(&mut buffer).unwrap();
        // send to Bob
        connection_a.write(&buffer).unwrap();

        let iv_b: Vec<u8> = buffer[..aes::BLOCKSIZE].to_vec();
        let msg_b: Vec<u8> = buffer[aes::BLOCKSIZE..2 * aes::BLOCKSIZE].to_vec();

        // decrypt it
        let _plain_b = aes::cbc_decrypt(&msg_b, &key, &iv_b);
    }

    // next attack is almost the same as previous one, with the difference
    // that we must send ACK (so implementation is skipped)
    // effects of injected (to Bob) g parameter:
    //   g = 1   -> B = 1
    //   g = p   -> B = 0
    //   g = p-1 -> B = (-1)**b // why?
    // in each case we should inject (to Bob) A = B

    // almost a copy of "algorithms::secure_remote_password()"
    pub fn secure_remote_password_mitm_dictionary() {
        #![allow(non_snake_case)]
        use std::thread;
        use std::sync::mpsc::{Sender, Receiver};
        use std::sync::mpsc::channel;

        // of course this list should be bigger
        let dictionary: Vec<&[u8]> = vec![
            b"some other secret password",
            b"admin1",
            b"some secret password",
        ];

        let N: BigUint = BigUint::from(2137_u32);
        let g: BigUint = BigUint::from(2_u32);
        let k: BigUint = BigUint::from(3_u32);

        let I: Vec<u8> = b"client identity".to_vec();
        let P: Vec<u8> = b"some secret password".to_vec();

        let (tx_mitm_bignum, rx_mitm_bignum): (Sender<BigUint>, Receiver<BigUint>) = channel();
        let (tx_client_bignum, rx_client_bignum): (Sender<BigUint>, Receiver<BigUint>) = channel();
        let (tx_mitm_bytes, rx_mitm_bytes): (Sender<Vec<u8>>, Receiver<Vec<u8>>) = channel();
        let (tx_client_bytes, rx_client_bytes): (Sender<Vec<u8>>, Receiver<Vec<u8>>) = channel();

        let mitm = thread::spawn({
            let (N, g, _k) = (N.clone(), g.clone(), k.clone());
            move || {
                let s: Vec<u8> = utils::random_bytes(16);

                let _I_c: Vec<u8> = rx_mitm_bytes.recv().unwrap();
                let A: BigUint = rx_mitm_bignum.recv().unwrap();

                let b: BigUint = rand::thread_rng().gen_biguint_range(&BigUint::from(1_u32), &N);
                let B: BigUint = g.modpow(&b, &N);
                let uH: Vec<u8> = hash::sha1(&utils::random_bytes(16));
                let u: BigUint = BigUint::from_bytes_le(&uH);

                tx_client_bytes.send(s.clone()).unwrap();
                tx_client_bignum.send(B.clone()).unwrap();
                tx_client_bignum.send(u.clone()).unwrap();

                let M_c: Vec<u8> = rx_mitm_bytes.recv().unwrap();
                //println!("mitm\t(M_c): {M_c:?}");

                tx_client_bytes.send([0x00].to_vec()).unwrap();

                // now iterate over dictionary and try to find match password
                for P in dictionary {
                    let xH: Vec<u8> = hash::sha1(
                        &s
                        .iter()
                        .chain(P.iter())
                        .cloned()
                        .collect::<Vec<u8>>()
                    );
                    let x: BigUint = BigUint::from_bytes_le(&xH);
                    let v: BigUint = g.modpow(&x, &N);

                    let S: BigUint = (&A * &v.modpow(&u, &N)).modpow(&b, &N);
                    let K: Vec<u8> = hash::sha1(&S.to_bytes_le());

                    let M: Vec<u8> = hash::hmac_sha1(&K, &s);

                    if M == M_c {
                        println!("mitm\t(P): {P:?} {}", String::from_utf8_lossy(&P));
                        break;
                    }
                }
            }
        });

        let client = thread::spawn({
            let (N, g, _k) = (N.clone(), g.clone(), k.clone());
            let (I, P) = (I.clone(), P.clone());
            move || {
                let a: BigUint = rand::thread_rng().gen_biguint_range(&BigUint::from(1_u32), &N);
                let A: BigUint = g.modpow(&a, &N);

                tx_mitm_bytes.send(I.clone()).unwrap();
                tx_mitm_bignum.send(A.clone()).unwrap();

                let s: Vec<u8> = rx_client_bytes.recv().unwrap();
                let B: BigUint = rx_client_bignum.recv().unwrap();
                let u: BigUint = rx_client_bignum.recv().unwrap();

                let xH: Vec<u8> = hash::sha1(
                    &s
                    .iter()
                    .chain(P.iter())
                    .cloned()
                    .collect::<Vec<u8>>()
                    );
                let x: BigUint = BigUint::from_bytes_le(&xH);

                let S: BigUint = B.modpow(&(a + u * x), &N);
                let K: Vec<u8> = hash::sha1(&S.to_bytes_le());

                let M: Vec<u8> = hash::hmac_sha1(&K, &s);

                tx_mitm_bytes.send(M.clone()).unwrap();

                let _result: Vec<u8> = rx_client_bytes.recv().unwrap();
            }
        });

        mitm.join().unwrap();
        client.join().unwrap();
    }
}

pub mod rsa {
    use crate::ciphers;
    use crate::utils;
    use crate::oracles;
    use crate::hash;
    use num_bigint::BigUint;

    pub fn unpadded_message_small_e() {
        // unknown plaintext
        let msg: Vec<u8> = utils::random_bytes(512);

        // the bigger k is, longer message we could decrypt
        let k: usize = 4;

        // generate k different public keys with e=3 (small)
        let pub_keys: Vec<(BigUint, BigUint)> = (0..k)
            .map(|_| ciphers::rsa::keygen_small_e().0)
            .collect();

        // encrypt same plaintext using them
        let ciphers: Vec<BigUint> = (0..k)
            .map(|i| BigUint::from_bytes_le(&ciphers::rsa::encrypt(&msg, &pub_keys[i])))
            .collect();

        let n: Vec<BigUint> = pub_keys
            .iter()
            .map(|(_, x)| x.clone())
            .collect();

        let prod_n: BigUint = n.iter().product();

        let s: Vec<BigUint> = n
            .iter()
            .map(|x| prod_n.clone() / x)
            .collect();

        // small exponent e allow us to solve system of small number (k) of
        // moduli equations for plain^e, because by taking enough different
        // n (pub_keys), we will cover whole range within which ciphertext can be

        // decrypt text using chinease remainder theorem
        // and since e=3, take cubic root
        let plain: Vec<u8> = ((0..k)
            .map(|i| &ciphers[i] * &s[i] * utils::modinv(&s[i], &n[i]))
            .sum::<BigUint>() % prod_n)
            .cbrt().to_bytes_le();

        assert!(msg == plain);
    }

    pub fn unpadded_message_recovery_oracle() {
        use oracles::rsa::unpadded_message_recovery_oracle_get;
        use oracles::rsa::unpadded_message_recovery_oracle_check;

        let (pub_key, priv_key) = ciphers::rsa::keygen();
        let (e, n) = pub_key.clone();

        // intercept message and its hash
        let (cipher, hash) = unpadded_message_recovery_oracle_get(&pub_key);

        // random value for scrambling hash only
        let s: BigUint = BigUint::from(2_u32);
        let payload: BigUint = s.modpow(&e, &n);

        // recoverably modify message to change hash
        let new_cipher: Vec<u8> = ((BigUint::from_bytes_le(&cipher) * &payload) % &n).to_bytes_le();

        // send to oracle modified message so hash will not match
        let new_plain: Vec<u8> = unpadded_message_recovery_oracle_check(&new_cipher, &hash, &priv_key).unwrap();

        // unscramble plain text
        let msg: Vec<u8> = ((BigUint::from_bytes_le(&new_plain) * utils::modinv(&s, &n)) % n).to_bytes_le();

        assert!(msg == ciphers::rsa::decrypt(&cipher, &priv_key));
    }

    pub fn bleichenbacher_signature_forge() {
        use oracles::rsa::bleichenbacher_signature_forge_check;

        let blocksize: usize = 1024;

        let (pub_key, priv_key) = ciphers::rsa::keygen_small_e();

        let msg: &[u8] = b"hi mom";
        // hardcode asn1 istead of compiling / parsing it
        let asn1_sha1: &[u8] = &[0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14];
        let hash: Vec<u8> = hash::sha1(msg);

        let data: Vec<u8> = vec![0x00]
            .iter()
            .chain(asn1_sha1.iter())
            .chain(hash.iter())
            .cloned()
            .collect();

        let mut signature: Vec<u8> = vec![0x00, 0x01];
        signature.extend(vec![0xff; blocksize / (u8::BITS as usize) - 2 - data.len()]);
        signature.extend(&data);

        // priv key for signing
        let encrypted_signature: Vec<u8> = ciphers::rsa::encrypt(&signature, &priv_key);
        assert!(bleichenbacher_signature_forge_check(&msg, &encrypted_signature, &pub_key));

        // set number of trailing zero bits, so we can put garbage there to obtain perfect cube
        // we are looking for perfect cube, because e=3
        let trailing_zeros = 712;
        // create signtaure in form of BigUint with shorter padding and trailing zeros at the end
        let mut forged_signature: BigUint = (BigUint::from(1_u32) << (blocksize - 15))
            - (BigUint::from(1_u32) << (trailing_zeros + data.len() * u8::BITS as usize))
            + (BigUint::from_bytes_be(&data) << trailing_zeros);

        // set forged signature to nearest perfect cube
        forged_signature = (forged_signature.cbrt()+1_u32).pow(3);

        // cube root works as signing
        let encrypted_forged_signature: Vec<u8> = forged_signature.cbrt().to_bytes_be();

        assert!(bleichenbacher_signature_forge_check(&msg, &encrypted_forged_signature, &pub_key))
    }

    pub fn parity_oracle() {
        use oracles::rsa::parity_oracle_check;

        let (pub_key, priv_key) = ciphers::rsa::keygen();
        let cipher_original: Vec<u8> = ciphers::rsa::encrypt(&base64::decode(b"VGhhdCdzIHdoeSBJIGZvdW5kIHlvdSBkb24ndCBwbGF5IGFyb3VuZCB3aXRoIHRoZSBGdW5reSBDb2xkIE1lZGluYQ==").unwrap(), &pub_key);
        let mut cipher: BigUint = BigUint::from_bytes_be(&cipher_original);

        let (e, n) = pub_key;

        // initial range (low inclusive, high exclusive)
        let (mut low, mut high) = (BigUint::from(0_u32), n.clone());
        while &low < &high {
            let mid = (&low + &high) >> 1;
            cipher = (cipher * BigUint::from(2_u32).modpow(&e, &n)) % &n;

            // because n is odd, if 2*plaintext is odd, it is wrapping mod n
            // else not, so our range is divided in two halves
            if parity_oracle_check(&cipher.to_bytes_be(), &priv_key) {
                high = mid;
            } else {
                low = mid + 1_u32;
            }
        }

        // error of around 64 +- 8 in last byte (?)
        let plain: Vec<u8> = low.to_bytes_be();
        println!("{}", String::from_utf8_lossy(&plain));
    }

    pub fn bleichenbacher_padding_oracle() {
        #![allow(non_snake_case)]

        use rand::Rng;
        use oracles::rsa::bleichenbacher_padding_oracle_check;

        let keysize: usize = 768;
        let k = keysize / (u8::BITS as usize);
        let msg = b"hi mom";

        let mut padding: Vec<u8> = vec![0x00, 0x02];
        padding.extend(
            &(2..(k-msg.len()-1))
                .map(|_| rand::thread_rng().gen_range(0x01..=0xff) as u8)
                .collect::<Vec<u8>>()
        );
        padding.push(0x00);

        let padded_msg: Vec<u8> = padding
            .iter()
            .chain(msg.iter())
            .cloned()
            .collect();

        let (pub_key, priv_key) = ciphers::rsa::keygen_custom_size(keysize);
        let cipher = BigUint::from_bytes_be(&ciphers::rsa::encrypt(&padded_msg, &pub_key));
        let (e, n) = pub_key;

        let B = BigUint::from(1_u32) << (u8::BITS as usize * (k - 2));

        // since cipher^d is pkcs1.5 conforming, we skip step 1
        let mut i = 1;
        let mut M: Vec<(BigUint, BigUint)> = vec![(2_u32 * &B, 3_u32 * &B - 1_u32)];
        let mut last_s = BigUint::from(1_u32);

        // continue search until M contains one interval of length 1
        while !(M.len() == 1 && M[0].0 == M[0].1) {
            if i == 1 {
                // step 2a
                let mut s: BigUint = &n / (3_u32 * &B);
                while !bleichenbacher_padding_oracle_check(
                    &(&cipher * s.modpow(&e, &n) % &n).to_bytes_be(),
                    &priv_key,
                    k
                ) {
                    s += 1_u32;
                }
                last_s = s;
            } else {
                if M.len() > 1 {
                    // step 2b
                    let mut s: BigUint = &last_s + 1_u32;
                    while !bleichenbacher_padding_oracle_check(
                        &(&cipher * s.modpow(&e, &n) % &n).to_bytes_be(),
                        &priv_key,
                        k
                    ) {
                        s += 1_u32;
                    }
                    last_s = s;
                } else {
                    // step 2c
                    let (a, b) = &M[0];
                    let mut rn: BigUint = 2_u32 * (b * &last_s - 2_u32 * &B);
                    let mut found = false;
                    while !found {
                        let mut s: BigUint = (2_u32 * &B + &rn) / b;
                        while s <= (3_u32 * &B + &rn) / a {
                            if bleichenbacher_padding_oracle_check(
                                &(&cipher * s.modpow(&e, &n) % &n).to_bytes_be(),
                                &priv_key,
                                k
                            ) {
                                last_s = s;
                                found = true;
                                break;
                            }

                            s += 1_u32;
                        }
                        rn += &n;
                    }
                }
            }

            // step 3
            let mut new_M: Vec<(BigUint, BigUint)> = Vec::new();
            for (a, b) in &M {
                let mut r: BigUint = (a * &last_s - 3_u32 * &B + 1_u32) / &n;
                while r <= (b * &last_s - 2_u32 * &B) / &n {
                    let tmp_a: BigUint = (2_u32 * &B + &r * &n + &last_s - 1_u32) / &last_s;
                    let tmp_b: BigUint = (3_u32 * &B - 1_u32 + &r * &n) / &last_s;

                    let interval = (std::cmp::max(a.clone(), tmp_a), std::cmp::min(b.clone(), tmp_b));
                    if interval.0 <= interval.1 {
                        new_M.push(interval);
                    }

                    r += 1_u32;
                }
            }
            M = new_M;
            i += 1;
        }

        // ramaining interval (of size 1) contains decrypted plaintext
        // also unpad message
        let plain: Vec<u8> = M[0].0.to_bytes_be()
            .iter()
            .skip_while(|&x| *x != 0x00)
            .skip(1)
            .cloned()
            .collect();

        assert!(plain == msg);
    }
}

pub mod dsa {
    use num_bigint::BigUint;
    use crate::hash;
    use crate::ciphers;
    use crate::utils;
    use crate::oracles;

    fn recover_x_from_k(k: &BigUint, msg: &[u8], signature: &(BigUint, BigUint)) -> BigUint {
        let (_p, q, _g) = ciphers::dsa::params();
        let (r, s) = signature;

        ((&q + s * k - BigUint::from_bytes_be(&hash::sha1(msg))) * utils::modinv(&r, &q)) % &q
    }

    pub fn key_recovery_from_nonce() {
        let (p, _q, g) = ciphers::dsa::params();
        let msg = b"For those that envy a MC it can be hazardous to your health\nSo be friendly, a matter of life and death, just like a etch-a-sketch\n";

        let y: BigUint = BigUint::parse_bytes(b"84ad4719d044495496a3201c8ff484feb45b962e7302e56a392aee4abab3e4bdebf2955b4736012f21a08084056b19bcd7fee56048e004e44984e2f411788efdc837a0d2e5abb7b555039fd243ac01f0fb2ed1dec568280ce678e931868d23eb095fde9d3779191b8c0299d6e07bbb283e6633451e535c45513b2d33c99ea17", 16).unwrap();

        let signature: (BigUint, BigUint) = (
            BigUint::parse_bytes(b"548099063082341131477253921760299949438196259240", 10).unwrap(),
            BigUint::parse_bytes(b"857042759984254168557880549501802188789837994940", 10).unwrap()
        );

        let mut x: BigUint = BigUint::from(0_u32);
        for k in 0..=(1<<16) as u32 {
            let test_x = recover_x_from_k(&BigUint::from(k), msg, &signature);

            if g.modpow(&test_x, &p) == y {
                println!("priv_key: {test_x}");
                x = test_x;
                break;
            }
        }

        assert!(
            hex::encode(hash::sha1(&hex::encode(x.to_bytes_be()).as_bytes())).as_bytes()
            == b"0954edd5e0afe5542a4adf012611a91912a3ec16"
        );
    }

    pub fn nonce_recovery_from_repeated_nonce() {
        use oracles::dsa::nonce_recovery_from_repeated_nonce_get;

        let (_p, q, _g) = ciphers::dsa::params();

        // msg, s, r, m
        let mut data = nonce_recovery_from_repeated_nonce_get();
        data.sort_by(|a, b| a.2.cmp(&b.2));

        // find messages with equal r
        // it will mean equal k also
        let mut i = 0;
        while i < data.len() - 1{
            if data[i].2 == data[i+1].2 {
                break;
            }
            i += 1;
        }
        assert!(i < data.len() - 1);

        // simple math to discover k
        let s = (data[i].1.clone(), data[i+1].1.clone());
        let m = (data[i].3.clone(), data[i+1].3.clone());

        let k = utils::modinv(&((&q + &s.0 - &s.1) * utils::modinv(&(&q + &m.0 - &m.1), &q)), &q);

        // and then x
        let x = recover_x_from_k(&k, &data[i].0, &(data[i].2.clone(), data[i].1.clone()));

        assert!(
            hex::encode(hash::sha1(&hex::encode(x.to_bytes_be()).as_bytes())).as_bytes()
            == b"ca8f6f7c66fa362d40760d135b763eb8527d3d52"
        );
    }

    pub fn parameter_tempering() {
        // generate malicious params
        let (p, q, _) = ciphers::dsa::params();
        let g = &p + 1_u32;

        // generate pub_key with default params
        let (_x, y) = ciphers::dsa::keygen(&ciphers::dsa::params());

        // arbitrary value for math only
        let z: BigUint = BigUint::from(2_u32);

        // create malicious signature
        let r: BigUint = y.modpow(&z, &p) % &q;
        let s: BigUint = (&r * utils::modinv(&z, &q)) % &q;

        // pair (r,s) is valid for any message if checked with pub_key y
        // and has been generated without knowledge of x
        assert!(ciphers::dsa::verify(b"", &y, &(r, s), &(p, q, g)));
    }
}
