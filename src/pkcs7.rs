pub fn pad(msg: &[u8], blocksize: usize) -> Vec<u8> {
    let mut result = Vec::from(msg);
    let padding_len = blocksize - (msg.len() % blocksize);
    result.extend(vec![padding_len as u8; padding_len]);

    result
}

pub fn check(msg: &[u8], blocksize: usize) -> bool {
    let last_byte = msg[msg.len() - 1];

    (0x01..=(blocksize as u8)).contains(&last_byte)
        && msg[msg.len() - (last_byte as usize)..]
            == vec![last_byte; last_byte as usize]
}

pub fn strip(msg: &[u8], blocksize: usize) -> Vec<u8> {
    if check(msg, blocksize) {
        let last_byte = msg[msg.len() - 1];
        msg[..msg.len() - (last_byte as usize)].to_vec()
    } else {
        msg.to_vec()
    }
}
